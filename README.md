[![pipeline status](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/commits/master)
Integration: [![coverage report](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/commits/master)

# wp-wpdesk-activation-reminder

## Developing

Before commit execute: `npm run prod` !

## Usage

```bash
composer require wpdesk/wp-wpdesk-activation-reminder

composer update
```

composer.json `extra` section in project must contain:
```
	"activation-reminder" : {
	  "plugin-title" : "Flexible Shipping PRO",
	  "plugin-dir" : "flexible-shipping-pro",
	  "logo-url": "assets/images/logo-fs.svg",
	  "buy-plugin-url": "https://flexibleshipping.com/products/flexible-shipping-pro-woocommerce/"
	}
```
where:
- `plugin-title` - plugin title displayed in activation reminder popup
- `plugin-dir` - plugin dir name - used to generate URLs for assets
- `logo-url` - logo path in plugin directory
- `buy-plugin-url` - URL to product page in shop

